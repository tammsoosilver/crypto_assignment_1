<?php

$text = "The most beautiful things in the world cannot be seen or touched they are felt with the heart";

$text = str_replace(' ', '', $text);
$text = str_split(strtoupper($text));

$count = [];

// Count characters in array of text
foreach ($text as $c) {
  if (!isset($count[$c])) $count[$c] = 0;

  $count[$c] += 1;
}

$count_new = [];
// Redo array of characters with count and add in characters, which were not found in text
for ($i=65; $i < 91 ; $i++) {
  $c = chr($i);
  $count_new[$c] = 0;
  if (isset($count[$c])) {
    $count_new[$c] = $count[$c];
  }
}

$freq = [];
// Calculate frequency for each character
foreach ($count_new as $key => $value) {
  $freq[$key] = round($value/26, 3);
}

// Calculate index of coincidence

// Initial IOC is 0
$ioc = 0;
// Number of different letters 26
$n = 26;

// Loop through each and every count of letters and add them to the total IOC
foreach ($count_new as $key => $value) {
  $ioc += ($value/$n) * (($value - 1)/($n - 1));
}

$ioc = round($ioc, 3);

echo $ioc;
