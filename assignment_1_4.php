<?php

function shift_cypher($text, $key) {

  // Return empty string on empty plaintext
  if (empty($text)) return ''; 

  // Normalize key if key is bigger than 25,
  // since we have 26 keys in total counting '0' key
  while ($key > 25) $key -= 26;
  
  // Create array of characters from plaintext
  $text = str_split(strtoupper($text));

  // Loop over each character in array
  for ($i=0; $i < sizeof($text); $i++) {

    // Get ASCII code of current character
    $ascii_code = ord($text[$i]);
    
    // Break if character is not uppercase letter
    if ($ascii_code > 90 || $ascii_code < 65) {
      echo $text[$i];
      throw new Exception("Error: Every character has to be in the English alphabet.", 1);
    }

    // Calculate replacement ascii value for character according to key
    $ascii_shifted = ord($text[$i]) + $key;

    // Only uppercase letters, if ascii code is over 90('Z'),
    // we go back to beginning and transfer the remainder
    if ($ascii_shifted > 90) {
      $ascii_shifted = $ascii_shifted - 90 + 64;
    }

    // Get letter from ascii code
    $text[$i] = chr($ascii_shifted);
  }

  // Recreate string from array
  return implode('', $text);
}

function permutation_cypher($text, $key) {

  // Get block size from the key since it is an array of numbers
  $block_size = sizeof($key);
  
  // Split the text into $block_size chunks
  $text = str_split($text, $block_size);

  // Loop over blocks and change order according to key
  for($i = 0; $i < sizeof($text); $i++) {
    
    // If a text block isn't large enough we pad with 'X'
    if (strlen($text[$i]) < $block_size) {
      $text[$i] = $text[$i] . str_repeat('X', $block_size - strlen($text[$i]));
    }

    // Convert the current block into an array of characters
    $text[$i] = str_split($text[$i]);

    // Create a buffer for the original text block
    $text_block_buf = $text[$i];

    // Permute the characters in the current block
    for($j = 0; $j < sizeof($text[$i]); $j++) {
      // Since we are working with PHP arrays, I'll remove 1 since the arrays
      // start at index 0
      $text[$i][$j] = $text_block_buf[$key[$j] - 1];
    }
    
    // Convert current text block into a string
    $text[$i] = implode('', $text[$i]);
  }
  
  // Convert array of text blocks into a string
  return implode('', $text);
}

$text = 'MOTIVATION';
$k_s1 = 17;
$k_s2 = 8;
$k_p1 = [5,1,3,2,4];
$k_p2 = [3,4,5,1,2];

// OUTPUT:
echo('Plaintext: '.$text);
echo("\n");
$s1 = shift_cypher($text, $k_s1);
echo('S1: '.$s1);
echo("\n");
$p1 = permutation_cypher($s1, $k_p1);
echo('P1: '.$p1);
echo("\n");
$s2 = shift_cypher($p1, $k_s2);
echo('S2: '.$s2);
echo("\n");
$p2 = permutation_cypher($s2, $k_p2);
echo('P2(Cyphertext): '.$p2);
echo("\n");

$cypher = $p2;
